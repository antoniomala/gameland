# -*- coding: utf-8 -*-
from django.db import models
from django.utils import timezone
from imagekit.models import ProcessedImageField
from imagekit.processors import ResizeToFill
from django.template.defaultfilters import slugify


class Categoria(models.Model):
    """ este model define as possíveis categorias de jogos, de modo a promover o agrupamento de título classificados por tipo, otimizando as pesquisas """

    # dados descritivos
    nome = models.CharField(u'Nome da categoria', max_length=100)
    descricao = models.TextField(u'Descrição', blank=True, null=True)

    # dados de controle e de auditoria
    status = models.BooleanField(u'Marque para categoria ativa', default=True)
    cadastrado_em = models.DateTimeField(auto_now_add=True, blank=True, null=True, verbose_name=u'Cadastrado em')
    modificado_em = models.DateTimeField(auto_now=True, blank=True, null=True, verbose_name=u'Alterado em')

    def __unicode__(self):
        return self.nome

    class Meta:
        ordering = ('nome',)



class Titulo(models.Model):
    """ este model tem como objetivo a existência do
     tipo Titulo no sistema, de modo a podermos adicionar por 
     meio dele cada título individual de jogo no sistema """

    # dados de referência
    # TODO - adicionar aqui referência ao usuário que efetue o cadastro no sistema (autor)
    categoria = models.ForeignKey(Categoria, related_name='titulos_na_categoria')

    # dados de controle e de auditoria
    status = models.BooleanField(u'Marque para titulo ativo', default=False)
    cadastrado_em = models.DateTimeField(auto_now_add=True, blank=True, null=True, verbose_name=u'Cadastrado em')
    modificado_em = models.DateTimeField(auto_now=True, blank=True, null=True, verbose_name=u'Alterado em')
    downloads = models.PositiveIntegerField(default=0)

    # dados descritivos
    nome = models.CharField(u'Nome do jogo', max_length=100, unique=True)
    descricao = models.TextField(u'Descrição')
    capa = ProcessedImageField(upload_to='testes', processors=[ResizeToFill(592, 318)], format='JPEG', options={'quality': 80})

    # cria um slug(apelido para o objeto) para utilizar na URL no lugar de somente o id
    slug = models.SlugField(max_length=400, blank=True,
                            null=True, editable=False)

    # o metodo save para criar um slug automaticamente 
    def save(self, *args, **kwargs):
        super(Teste, self).save(*args, **kwargs)
        slug = "%s-%s" % (slugify(self.nome), self.id)
        if not self.slug or self.slug != slug:
            self.slug = slug
            self.save()

    def __unicode__(self): return self.nome

    class Meta:
        ordering = ('nome', 'categoria',)