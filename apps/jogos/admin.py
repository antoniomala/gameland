# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import Categoria, Titulo

class CategoriaAdmin(admin.ModelAdmin):
    list_display = ('nome', )

class TituloAdmin(admin.ModelAdmin):
    list_display = ('nome', 'cadastrado_em')
    list_filter = ('categoria',)

admin.site.register(Categoria)
admin.site.register(Titulo, TituloAdmin)
