# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.http import HttpResponseRedirect

# Create your views here.
def home(request):
	return render(request, 'home.html', {})

def quem_somos(request):
	return render(request, 'quem_somos.html', {})

def jogo(request):
	return render(request, 'jogo.html', {})

def jogos(request):
	return render(request, 'jogos.html', {})

def empresa(request):
	return render(request, 'empresa.html', {})

def contato(request):
	return render(request, 'contato.html', {})

def gamelanders(request):
	return render(request, 'gamelanders.html', {})

def gamelander(request):
	return render(request, 'gamelander.html', {})