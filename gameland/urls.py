from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    # Examples:
    # url(r'^$', 'gameland.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^accounts/', include('allauth.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'apps.website.views.home', name='home'),

    url(r'^contato$', 'apps.website.views.contato', name='contato'),
    url(r'^gamelanders$', 'apps.website.views.gamelanders', name='gamelanders'),
    url(r'^quem_somos$', 'apps.website.views.quem_somos', name='quem_somos'),
    url(r'^jogo/(?P<jogo>\d+)$', 'apps.website.views.jogo', name='jogo'),
    url(r'^jogo/$', 'apps.website.views.jogo', name='jogo'),
    url(r'^jogos/$', 'apps.website.views.jogos', name='jogos'),
    url(r'^gamelander/$', 'apps.website.views.gamelander', name='gamelander'),

] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)